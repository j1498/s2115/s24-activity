db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"department": "HR",
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"department": "HR",
	},
	{
		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"email": "billgates@mail.com",
		"department": "Operations",
	},
	{
		"firstName": "Jane",
		"lastName": "Doe",
		"age": 21,
		"email": "janedoe@mail.com",
		"department": "HR",
	}

	]);


// 3. Find Users $or operator

db.users.find({
	$or: [
		{
			"firstName": {
				$regex: 'S'
			}
		},
		{
			"lastName":{
				$regex: 'D'
			}
		}
	] 
},
{
	"_id": 0,
	"firstName": 1,
	"lastName": 1
});


// 4. Find Users $and operator

db.users.find(
{
	$and:[
		{
			"department": "HR"
		},
		{
			"age": {$gte: 70}
		}
	]
	
});



// 5. Find Users $and, $regex, $lte operator

db.users.find({
	$and:[
		{
			"firstName": {
				$regex: 'e'
			}
		},
		{
			"age": {
				$lte: 30
			}
		}

	]
});



		