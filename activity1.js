// Add Users

db.users.insertMany([
{
	"firstName": "Diane",
	"lastName": "Murphy",
	"email": "dmurphy@mail.com",
	"isAdmin": false,
	"isActive": true
},
{
	"firstName": "Mary",
	"lastName": "Patterson",
	"email": "mpatterson@mail.com",
	"isAdmin": false,
	"isActive": true
},
{
	"firstName": "Jeff",
	"lastName": "Firelli",
	"email": "jfirrelli@mail.com",
	"isAdmin": false,
	"isActive": true
},
{
	"firstName": "Gerard",
	"lastName": "Bondur",
	"email": "gbondur@mail.com",
	"isAdmin": false,
	"isActive": true
},
{
	"firstName": "Pamela",
	"lastName": "Castillo",
	"email": "pcastillo@mail.com",
	"isAdmin": true,
	"isActive": false
},
{
	"firstName": "George",
	"lastName": "Vanauf",
	"email": "gvanauf@mail.com",
	"isAdmin": true,
	"isActive": true
}

	]);

// Add Courses

db.courses.insertMany([
{
	"name": "Professional Development",
	"price": 1000
},
{
	"name": "Business Processing",
	"price": 13000
}

	]);


// Find Method

db.users.find({
	"isAdmin": false
});


// Update Method

db.courses.updateMany({},
{
	$set: {
		"enrollees": [
			{
				"userid": ObjectId("620cdce13eed6912d5373fee") 
			},
			{
				"userid": ObjectId("620cdce13eed6912d5373fef") 
			},
			{
				"userid": ObjectId("620cdce13eed6912d5373ff0")
			},
			{
				"userid": ObjectId("620cdce13eed6912d5373ff1")
			},
			{
				"userid": ObjectId("620cdce13eed6912d5373ff2") 
			},
			{
				"userid": ObjectId("620cdce13eed6912d5373ff3")
			}

		]
	}
});
